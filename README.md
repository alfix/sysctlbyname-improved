sysctlbyname-improved
=====================

A new sysctl internal object to convert a sysctl name to the corresponding
OID and to improve
[sysctlbyname(3)](https://man.freebsd.org/sysctlbyname/3).

Introduction
------------

The FreeBSD kernel maintains a Management Information Base (MIB) where a 
component (object) represents a parameter of the system. The sysctl() system 
call explores the MIB to find an object by its Object Identifier (OID) and 
calls its handler to get or set the value of the parameter.

The [sysctlbyname(3)](https://man.freebsd.org/sysctlbyname/3) manual declares:

	"The sysctlbyname() function accepts an ASCII representation of the name
	and internally looks up the integer name vector.  Apart from that, it be-
	haves the same as the standard sysctl() function"

The purpose of sysctlbyname-improved is to allow sysctlbyname() to handle:

 - a name with some level name equals to the empty string character, example 
   "security.jail.param.allow.mount."
 - a CTLTYPE\_NODE with a defined handler, example "kern.proc.pid.\<pid\>"

An improved sysctlbyname() clone is available in
[examples/sysctlbyname\_improved.c](examples/sysctlbyname\_improved.c).

Getting Started
---------------

To install the port 
([sysutils/sysctlbyname-improved-kmod](https://www.freshports.org/sysutils/sysctlbyname-improved-kmod/)):
```
# cd /usr/ports/sysutils/sysctlbyname-improved-kmod/ && make install clean
```
To add the package:
```
# pkg install sysctlbyname-improved-kmod
```
Example to use sysctlbyname-improved:
```
% git clone https://gitlab.com/alfix/sysctlbyname-improved.git/
% cd sysctlbyname-improved
% make
% sudo make load
% cd examples
% cc sysctlbyname_improved.c example.c
% ./a.out
  ## sysctlbyname ##
security.jail.param.allow.mount.: error
kern.proc.pid.1: error
  ## sysctlbyname_improved ##
security.jail.param.allow.mount.:  0
kern.proc.pid.1: (effective user id) 0
```

API
---

**To get and set the value of a sysctl object by its name**

```c
int
sysctlbyname_improved(const char *name, void *oldp, size_t *oldlenp,
    const void *newp, size_t newlen);
```

**To get the OID of an object by its name**

```c
#define OBJIDEXTENDED_BYNAME 10
/* if sysctlinfo is not installed */
#define SYSCTLINFO_BYNAME(name, prop, buf, buflen) \
	sysctl(prop, 2, buf, buflen, name, strlen(name) + 1)


int id[CTL_MAXNAME], op[2] = {CL_SYSCTL, OBJIDEXTENDED_BYNAME};
size_t idlevel = CTL_MAXNAME * sizeof(int);

error = SYSCTLINFO_BYNAME(name, op, id, &idlevel);
idlevel /= sizeof(int);
```

The name can include the input for the object, example: 
_"kern.proc.pid.\<pid\>"_ -> [1.14.1.\<pid\>] or 
_"name1.name2.name3.inp1,inp2.inp3"_ -> [100.200.300.\<inp1\>.\<inp2\>.\<inp3\>]. 
If an input is not supplied the behavior of OBJIDEXTENDED\_BYNAME is like that of 
OBJIDBYNAME of sysctlinfo. Note, the input is different from the new value 
_newp_, the input is passed extending the name (or the OID for sysctl(3)).

 - new feature (the kernel has not this feature)
 - KASSERT(9) object has the CTLFLAG\_DYING flag
 - [ENOENT] error: object is CTLTYPE\_NODE and has the CTLFLAG\_DORMANT flag
 - [ENAMETOOLONG] error: length of name >= MAXPATHLEN
 - [EINVAL] error: if name has more than CTL\_MAXNAME levels
 - [EINVAL] error: some \<inp\> is not a number
 - [ENOENT] error: object does not exist
 - [ENOENT] error: object has an input but it is not a CTLTYPE\_NODE with 
   a no-NULL handler.
 - [ECAPMODE] error: object has not CTLFLAG\_CAPRD or CTLFLAG\_CAPWR in 
   capability mode


**High level API to get OID by name**

The [sysctlmibinfo2](https://gitlab.com/alfix/sysctlmibinfo2) library provides:
```c
int sysctlmif_oidextendedbyname(const char *name, int *id, size_t *idlevel);
```

Implementation note
-------------------

sysctlbyname\_improved() is implemented by a new internal object,
*sysctl.objidextended\_byname*, to get an OID by a name eventually expanded
with an input for the handler.The internal object uses the code of the
[sysctlinfo interface](https://gitlab.com/alfix/sysctlinfo), however the kernel
modules for sysctlbyname-improved and the sysctlinfo are independent of each
other:
```
% kldstat | grep sysctl
 2    1 0xffffffff8253a000     41c8 sysctlinfo.ko
 4    1 0xffffffff8254b000     1d28 sysctlbyname_improved.ko
```

