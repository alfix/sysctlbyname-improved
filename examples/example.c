/*-
 * SPDX-License-Identifier: CC0-1.0
 *
 * Written in 2019 by Alfonso Sabato Siciliano.
 * To the extent possible under law, the author has dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty, see:
 *   <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <sys/types.h>
#include <sys/sysctl.h>
#include <sys/user.h>

#include <stdio.h>
#include <string.h>

#include "sysctlbyname_improved.h"

/*
   % cc sysctlbyname_improved.c example.c
   % ./a.out
*/
int main()
{
	int error, value;
	size_t valuelen;;
	struct kinfo_proc kp;

	printf("  ## sysctlbyname ##\n");
	valuelen = sizeof(int);
	if (sysctlbyname("security.jail.param.allow.mount.", &value, &valuelen, NULL, 0) == 0)
		printf("security.jail.param.allow.mount.: %d\n", value);
	else
		printf("security.jail.param.allow.mount.: error\n");

	valuelen = sizeof(kp);
	if (sysctlbyname("kern.proc.pid.1", &kp, &valuelen, NULL, 0) == 0)
		printf("effective user id: %d\n", kp.ki_uid);
	else
		printf("kern.proc.pid.1: error\n");


	printf("  ## sysctlbyname_improved ##\n");
	valuelen = sizeof(int);
	if (sysctlbyname_improved("security.jail.param.allow.mount.", &value, &valuelen, NULL, 0) == 0)
		printf("security.jail.param.allow.mount.:  %d\n", value);
	else
		printf("security.jail.param.allow.mount.: error\n");

	valuelen = sizeof(kp);
	if (sysctlbyname_improved("kern.proc.pid.1", &kp, &valuelen, NULL, 0) == 0)
		printf("kern.proc.pid.1: (effective user id) %d\n", kp.ki_uid);
	else
		printf("kern.proc.pid.1: error\n");

	return 0;
}
