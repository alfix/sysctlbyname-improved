/*-
 * SPDX-License-Identifier: CC0-1.0
 *
 * Written in 2019 by Alfonso Sabato Siciliano.
 * To the extent possible under law, the author has dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty, see:
 *   <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <sys/types.h>
#include <sys/sysctl.h>

#include <string.h>

/* for FreeBSD < 13 */
#ifndef CTL_SYSCTL
#define CTL_SYSCTL	0
#endif

#define OBJIDEXTENDED_BYNAME	10
/* if sysctlinfo is not installed */
#define SYSCTLINFO_BYNAME(name, prop, buf, buflen) \
	sysctl(prop, 2, buf, buflen, name, strlen(name) + 1)

int
sysctlbyname_improved(const char *name, void *oldp, size_t *oldlenp,
    const void *newp, size_t newlen)
{
	int oid[CTL_MAXNAME], prop[2] = {CTL_SYSCTL, OBJIDEXTENDED_BYNAME};
	size_t oidlevel = CTL_MAXNAME * sizeof(int);

	if (SYSCTLINFO_BYNAME(name, prop, oid, &oidlevel) != 0)
		return (-1);
	oidlevel /= sizeof(int);
	
	return (sysctl(oid, oidlevel, oldp, oldlenp, newp, newlen));
}
