#include <sys/types.h>
#include <sys/sysctl.h>

#include <stdio.h>
#include <string.h>

#include "../sysctlinfo.h"

#define BUFSIZE    1024

int main()
{
	int id[CTL_MAXNAME], idnext[CTL_MAXNAME], name2id[CTL_MAXNAME], op[2];
	size_t idlevel, idnextlevel, buflen, name2idlevel, i;
	char buf[BUFSIZE];

	op[0] = CTL_SYSCTL;

	id[0]=1;
	id[1]=1;
	idlevel=2;

	for (;;) {
//		if(idlevel < 22)
//			goto next;

		printf("id [%zu]: ",idlevel);
		for (i = 0; i < idlevel; i++) {
			printf("%d%c", id[i], i+1 < idlevel ? '.' : '\n');
		}

		/* name */
		op[1] = OBJNAME;
		buflen = BUFSIZE;
		memset(buf, 0, sizeof(buf));
		if (sysctl(op, 2, buf, &buflen, id, idlevel * sizeof(int)) != 0) {
			printf("Error 'name'\n");
			return (1);
		}
		printf("name [%zuB]: %s\n", buflen, buf);

		/* nametoid */
		op[1] = OBJIDEXTENDED_BYNAME; // ENTRYIDBYNAME;
		name2idlevel = CTL_MAXNAME * sizeof(int);
		memset(name2id, 0, name2idlevel);
		if (sysctl(op, 2, name2id, &name2idlevel, buf, buflen) != 0) {
			printf("Error 'name2id'\n");
			return (1);
		}
		name2idlevel /= sizeof(int);
		printf("nametoid: ");
		for (i = 0; i < name2idlevel; i++) {
			printf("%d%c", name2id[i], i+1 < name2idlevel ? '.' : '\n');
		}
		
		if(idlevel != name2idlevel) {
			printf("idlevel != name2idlevel\n");
			return (1);
		}
		
		for(i=0; i < idlevel; i++) {
			if(id[i] != name2id[i]) {
				printf("idlevel[%zu] != name2id[%zu]\n", i, i);
				return (1);
			}
		}
		
		printf("-------------------------------------------\n");

//next:
		/* nextleaf (or nextnode) */
		idnextlevel = CTL_MAXNAME * sizeof(int);
		op[1] = NEXTOBJNODE;
		if (sysctl(op, 2, idnext, &idnextlevel, id, idlevel * sizeof(int)) != 0) {
			printf("Error next() or no next\n");
			break;
		}
		memcpy(id, idnext, idnextlevel);
		idlevel = idnextlevel / sizeof(int);
	}

	return (0);
}
