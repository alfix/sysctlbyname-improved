#include <sys/types.h>
#include <sys/sysctl.h>

#include <stdio.h>
#include <string.h>

#include "../sysctlinfo.h"

void printf_oid(int *id, size_t idlevel) {
	int i;
	
	printf("id: ");
	for (i = 0; i < idlevel; i++)
		printf("%d%c", id[i], i+1 < idlevel ? '.' : '\n');
}

int main()
{
	int op[2] = {CTL_SYSCTL, OBJIDEXTENDED_BYNAME}, id[CTL_MAXNAME], error;
	size_t idlevel = CTL_MAXNAME * sizeof(int);
	
	printf("Positive\n\n");

	/* test */
	error = SYSCTLINFO_BYNAME("security.jail.param.allow.mount.", op, id, &idlevel);
	if (error != 0)
		printf("error: security.jail.param.allow.mount.\n");
	else
		printf_oid(id, idlevel / sizeof(int));
	
	/* test */
	idlevel = CTL_MAXNAME * sizeof(int);
	error = SYSCTLINFO_BYNAME("c1.c2.c3.c4.c5.c6.c7.c8.c9.c10.c11.c12.c13.c14."\
				  "c15.c16.c17.c18.c19.c20.c21.c22.c23.c24", op, id, &idlevel);
	if (error != 0)
		printf("error: c1. ... .c24\n");
	else
		printf_oid(id, idlevel / sizeof(int));
	
	/* test */
	idlevel = CTL_MAXNAME * sizeof(int);
	error = SYSCTLINFO_BYNAME("kern.proc.pid.1", op, id, &idlevel);
	if (error != 0)
		printf("error: kern.proc.pid.1\n");
	else
		printf_oid(id, idlevel / sizeof(int));
		
	/* test */
	idlevel = CTL_MAXNAME * sizeof(int);
	error = SYSCTLINFO_BYNAME("kern.proc.pid.4.5.6.7.8.9.10.11.12.13.14.15.16."\
				  "17.18.19.20.21.22.23.24", op, id, &idlevel);
	if (error != 0)
		printf("error: kern.proc.pid.1. ... .24\n");
	else
		printf_oid(id, idlevel / sizeof(int));

	printf("\n\nNegative\n\n");
	
	/* test */
	idlevel = CTL_MAXNAME * sizeof(int);
	error = SYSCTLINFO_BYNAME("c1.c2.c3.c4.c5.c6.c7.c8.c9.c10.c11.c12.c13.c14."\
				  "c15.c16.c17.c18.c19.c20.c21.c22.c23.c24.c25", op, id, &idlevel);
	if (error != 0)
		printf("error: c1. ... .c25\n");
	else
		printf_oid(id, idlevel / sizeof(int));
	
	/* test */
	idlevel = CTL_MAXNAME * sizeof(int);
	error = SYSCTLINFO_BYNAME("kern.proc.pid.4.5.6.7.8.9.10.11.12.13.14.15.16."\
				  "17.18.19.20.21.22.23.24.25", op, id, &idlevel);
	if (error != 0)
		printf("error: kern.proc.pid.4. .25\n");
	else
		printf_oid(id, idlevel / sizeof(int));
	
	/* test */
	idlevel = CTL_MAXNAME * sizeof(int);
	error = SYSCTLINFO_BYNAME("error", op, id, &idlevel);
	if (error != 0)
		printf("error: error\n");
	else
		printf_oid(id, idlevel / sizeof(int));
		
	/* test */
	idlevel = CTL_MAXNAME * sizeof(int);
	error = SYSCTLINFO_BYNAME("kern.ostype.1", op, id, &idlevel);
	if (error != 0)
		printf("error: kern.ostype.1\n");
	else
		printf_oid(id, idlevel / sizeof(int));
	
	/* test */
	idlevel = CTL_MAXNAME * sizeof(int);
	error = SYSCTLINFO_BYNAME("100.200.300.400.500.600", op, id, &idlevel);
	if (error != 0)
		printf("error: 100.200.300.400.500.600\n");
	else
		printf_oid(id, idlevel / sizeof(int));
		
	/* test */
	idlevel = CTL_MAXNAME * sizeof(int);
	error = SYSCTLINFO_BYNAME("kern.proc.pid.error.1", op, id, &idlevel);
	if (error != 0)
		printf("error: kern.proc.pid.error.1\n");
	else
		printf_oid(id, idlevel / sizeof(int));

	return 0;
}

